import React from "react";

import TodoListItem from "../todo-list-item/todo-list-item";
import classes from "./todo-list.module.css";

const TodoList = ({ todos, onDeleted, onToggleImportant, onToggleDone }) => {
  const elements = todos.map(item => {
    const { id, ...itemProps } = item;

    return (
      <li key={id} className={`list-group-item ${classes.listGroupItem}`}>
        <TodoListItem
          {...itemProps}
          onDeleted={() => onDeleted(id)}
          onToggleImportant={() => onToggleImportant(id)}
          onToggleDone={() => onToggleDone(id)}
        />
      </li>
    );
  });

  return <ul className={`list-group ${classes.todoList}`}>{elements}</ul>;
};

export default TodoList;
