import React, { Component } from "react";

import classes from "./item-add-form.module.css";
export default class ItemAddForm extends Component {
  state = {
    newText: "",
    placeHolder: "Введите текст для новой задачи",
    isTrue: false
  };
  getNewTodo = e => {
    this.setState({
      newText: e.target.value
    });
  };

  onSubmit = e => {
    e.preventDefault();
    if (this.state.newText === "") {
      this.setState({
        placeHolder: "Поле не должно быть пустым",
        isTrue: true
      });

      return;
    }

    this.props.onAdded(this.state.newText);
    this.setState({
      newText: " "
    });
  };

  render() {
    let placeHolderColor = `${"form-control"}`;
    if (this.state.isTrue === true) {
      placeHolderColor += ` ${classes.red}`;
    }
    return (
      <form
        className={`d-flex ${classes.itemAddForm}`}
        onSubmit={this.onSubmit}
      >
        <input
          className={placeHolderColor}
          type="text"
          placeholder={this.state.placeHolder}
          onChange={this.getNewTodo}
          value={this.state.newText}
        />
        <button className="btn btn-success">Добавить</button>
      </form>
    );
  }
}
