import React from "react";

import classes from "./search-panel.module.css";

const SearchPanel = () => {
  return (
    <input
      type="text"
      className={`form-control ${classes.searchInput}`}
      placeholder="Поиск по задачам"
    />
  );
};

export default SearchPanel;
