import React, { Component } from "react";

import classes from "./app.module.css";
import AppHeader from "../app-header/app-header";
import SearchPanel from "../search-panel/search-panel";
import TodoList from "../todo-list/todo-list";
import ItemStatusFilter from "../item-status-filter/item-status-filter";
import ItemAddForm from "../item-add-form/item-add-form";

export default class App extends Component {
  mainId = 1488;

  state = {
    todoData: [
      this.createTodoItem("Задача 1"),
      this.createTodoItem("Задача 2"),
      this.createTodoItem("Задача 3")
    ]
  };

  createTodoItem(label) {
    return {
      label,
      important: false,
      id: (this.mainId += 123)
    };
  }

  addItem = text => {
    const newItem = this.createTodoItem(text);

    this.setState(({ todoData }) => {
      const newArray = [...todoData, newItem];

      return {
        todoData: newArray
      };
    });
  };

  deleteItem = id => {
    this.setState(({ todoData }) => {
      const idx = todoData.findIndex(el => el.id === id);

      return {
        todoData: [...todoData.slice(0, idx), ...todoData.slice(idx + 1)]
      };
    });
  };

  toggleProps(arr, id, propNames) {
    const idx = arr.findIndex(el => el.id === id);
    const oldItem = arr[idx];
    const newItem = { ...oldItem, [propNames]: !oldItem[propNames] };
    return [...arr.slice(0, idx), newItem, ...arr.slice(idx + 1)];
  }

  onToggleImportant = id => {
    this.setState(({ todoData }) => {
      return { todoData: this.toggleProps(todoData, id, "important") };
    });
  };

  onToggleDone = id => {
    this.setState(({ todoData }) => {
      return { todoData: this.toggleProps(todoData, id, "done") };
    });
  };
  render() {
    const { todoData } = this.state;
    const doneCount = todoData.filter(el => el.done).length;
    const todoCount = todoData.length - doneCount;
    return (
      <div className={classes.todoApp}>
        <AppHeader toDo={todoCount} done={doneCount} />
        <div className={`d-flex ${classes.topPanel}`}>
          <SearchPanel />
          <ItemStatusFilter />
        </div>

        <TodoList
          todos={todoData}
          onDeleted={this.deleteItem}
          onToggleImportant={this.onToggleImportant}
          onToggleDone={this.onToggleDone}
        />
        <ItemAddForm onAdded={this.addItem} />
      </div>
    );
  }
}
