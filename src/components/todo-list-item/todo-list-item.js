import React from "react";

import classes from "./todo-list-item.module.css";

const TodoListItem = ({
  label,
  onDeleted,
  onToggleImportant,
  onToggleDone,
  done,
  important
}) => {
  let classNames = `${classes.todoListItem}`;
  if (done) {
    classNames += ` ${classes.done}`;
  }
  // console.log(onToggleDone);
  if (important) {
    classNames += ` ${classes.important}`;
  }
  return (
    <span className={classNames}>
      <span
        className={classes.todoListItemLabel}
        onClick={id => onToggleDone(id)}
      >
        {label}
      </span>

      <button
        type="button"
        className="btn btn-outline-success btn-sm float-right"
        onClick={onToggleImportant}
      >
        <i className="fa fa-exclamation" />
      </button>

      <button
        type="button"
        className="btn btn-outline-danger btn-sm float-right"
        onClick={onDeleted}
      >
        <i className="fa fa-trash-o" />
      </button>
    </span>
  );
};

export default TodoListItem;
