import React from "react";
import classes from "./app-header.module.css";

const AppHeader = ({ toDo, done }) => {
  return (
    <div className={`d-flex ${classes.appHeader}`}>
      <h1>Todo List</h1>
      <div>
        <h2>{toDo} не выполненные</h2>
        <h2>{done} Выполненные</h2>
      </div>
    </div>
  );
};

export default AppHeader;
